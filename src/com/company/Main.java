package com.company;

import org.jetbrains.annotations.NotNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    // Метод распаковки отдельного(одного) пакета
    public static @NotNull String unpackingPackage(int numberOfRepetitions, String repeatableString){

        StringBuilder unpackedString = new StringBuilder();

        // Получаем кол-во повторений подстроки пакета и саму подстроку, далее конкатенируем подстроки сколько раз необходимо
        for (int i=0; i<numberOfRepetitions;i++)
            unpackedString.append(repeatableString);

        return unpackedString.toString();
    }

    // Метод распаковки строки с множеством пакетов
    public static @NotNull String unpackingStringOfPackages(String stringOfPackages){

        // Регулярное выражение одного пакета
        String packageRegex = "([2-9]+)\\[([a-zA-Z]+)\\]";
        Pattern packagePattern = Pattern.compile(packageRegex);
        Matcher packageMatcher = packagePattern.matcher(stringOfPackages);

        // Поиск пакетов с помощью регулярного выражения
        while (packageMatcher.find()){

            // В первой группе регулярного выражения - колчество повторений подстроки
            int numberOfRepetitions = Integer.parseInt(packageMatcher.group(1));

            // Во второй группе регулярного выражения - сама подстрока
            String repeatableString = packageMatcher.group(2);

            // Распаковываем найденный пакет
            String currentUnpackedPackage = unpackingPackage(numberOfRepetitions,repeatableString);

            // Обновляем нашу строку (подменяем подстроку с пакетом на распакованую подстроку)
            stringOfPackages = stringOfPackages.replace(packageMatcher.group(),currentUnpackedPackage);

            // Обновляем matcher, т.к. строка с пакетами изменилась
            packageMatcher = packagePattern.matcher(stringOfPackages);
        }

        // Проверяем на валидность входных данных: если в результативной строке остались лишь латинские символы
        // То либо пакетов не было вовсе, либо пакеты были и они успешно распакованы
        // Выводим результат, либо ошибку о невалидности данных
        if (stringOfPackages.matches("[a-zA-Z]+"))
            return stringOfPackages;
        else return "Invalid input string";
    }

    public static void main(String[] args) {

        // Массив строк с пакетами (для теста)
        String[] packedStrings = new String[] {"3[xyz]4[xy]z",
                "2[3[x]y]",
                "2[2[3[xy]qq4[z]]3[k]p]",
                "2[3[3x]y]",
                "2[=3[x]y]",
                "2[=3[.x]y]"};

        // Вывод результатов работы программы
        for (String packedString: packedStrings)
            System.out.printf("Input: %s\nOutput: %s\n\n",packedString,unpackingStringOfPackages(packedString));
    }
}
